# Grafana

## Installer Grafana

```bash
sudo apt install grafana
sudo systemctl restart grafana-server
```

## Fichiers de Configuration

Config dans `/etc/grafana/grafana.ini`

Site web dans `/usr/share/grafana/public/`

BDD sqlite dans `/var/lib/grafana/grafana.db` (utile pour retrouver les identifiants de connexion)

## Ajouter une source de données

Aller dans `Home` > `Administration` > `Data sources` > `Add data source`

Puis choisir `Prometheus` ou `InfluxDB`

## Ajouter un dashboard

Aller dans `Home` > `Dashboard` > `new` >`import`

ID du dashboard pour prometheus : `1860`