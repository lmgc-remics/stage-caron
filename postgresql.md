# PostgreSQL

## Installation

```shell
sudo apt install postgresql
```

## Acces au profil Postgres

```shell
sudo -i -u postgres
psql
```

Ou pour accéder directement à la console sql

```shell
sudo -u postgres psql
```

pour avoir des infos sur la bdd

    \c pour connaitre la bdd utilisé + l'utilisateur connecté ou pour se connecter avec un autre utilisateur
    \l pour lister les bdd
    \du pour lister les roles
    \d pour lister les tables

## Rendre accessible le service depuis l'exterieur

```shell
sudo nano /etc/postgres/14/main/postgresql.conf
```

changez la ligne

    #listen_addresses = 'localhost'

de cette manière

    listen_addresses = '*'
    ou
    listen_addresses = 'localhost, 162.38.53.151'

avec 162.38.53.151 l'ip du serveur de bdd

puis redemarrer pour prendre en compte

```shell
sudo service postgresql restart
```

## Ajouter les droit aux utilisateurs postgres

```shell
sudo nano /etc/postgres/14/main/pg_hba.conf
```

en ajoutant les lignes suivantes (les lignes sont interprétées dans l'ordre du fichier)


    host    bdd_one    admin_one   162.38.53.128/25    md5
    host    bdd_two    admin_two   162.38.53.128/25    md5
    host    bdd_two    user_two    162.38.53.128/25    md5

puis redemarrer pour prendre en compte

```shell
sudo service postgresql restart
```

## Creer des utilisateurs et gérer les droits

creation role/user (terminal sql)
    
    - create user user_one login password 'mypassverysure';
    - create database bdd_one;
    - grant all on bdd_one to user_one;

creation user (bash postgres account)

    - createuser user_one;

se connecter avec un role/user

```shell
psql -U user_one -h localhost -W bdd_one
```

## En pratique

**cf [SQL/creation_bdd.sql](./SQL/creation_bdd.sql)**