
# Stage d'IUT de Florimond Caron

Usefull links to understand how to set up bridge link
for the VM :
- https://unix.stackexchange.com/questions/572261/virtual-network-bridge-why-does-it-have-to-have-an-ip-address-assigned-to-it
- https://linuxconfig.org/how-to-use-bridged-networking-with-libvirt-and-kVM

Prometheus documentation
- https://www.mytinydc.com/
- https://xavki.blog/prometheus-grafana-tutoriaux-francais/
- https://gitlab.com/xavki/presentation-prometheus-grafana

Black list Adresse IP
- https://feodotracker.abuse.ch/blocklist/


## Déroulement

1. Installer [KVM](./kvm.md).

2. Configurer un bridge sur l'hote pour faire communiquer les VM sur le réseau.

3. Installer et configurer [SSH](./ssh.md) sur les VM pour utiliser uniquement les authentification par clées.
Restreindre les acces ssh des VM.

4. Préparer 1 VM BDD sous [PostgreSQL](./postgresql.md).
Créer 2 utilisateur et 2 BDD avec une gestion des droits séparés.

5. Préparer 1 VM Appli qui communique avec la BDD comme par exemple un serveur web sous [Apache](./apache.md) avec une page de login.

6. Mettre en place un [Fail2ban](./fail2ban.md) sur les connections SSH des VM.
Faire en sorte que mon Appli de login produise des logs.
Mettre un fail2ban sur mon Appli de login avec envoie de mail automatique a chaque bannissement.

7. Identifier des services (filtre existant de fail2ban).

8. Faire des stats sur les [logs](./log_analyse.md).

9. Identifier des black list d'adresse ip (site recommandé style ANSSI) pour automatiser avec un cron et des iptables.

10. Tester le backup des VM et leur restauration.

11. Monter une image qcow2 d'une VM en local pour pouvoir la modifier et mettre a jour ses paquets.

12. Agrandir la taille d'une image qcow2 et utiliser ce nouvel espace en utilisant l'espace libre avec les volumes de la VM.

13. Automatiser la création et le deploiement des VM avec des scripts ainsi qu'avec [Ansible](./ansible.md).

14. Mettre en place de la metrologie sur le hardware des serveurs avec [Prometheus](./prometheus.md) en reperant les données pertinent sur lesquels s'allerter
Utiliser un rendu graphique des données avec Grafana.

15. Configurer Alertmanager avec SSMTP en utilisant le serveur de mail de l'IUT pour envoyer des alertes en fcontion d'une condition sur les données de prometheus.

16. Mettre en place une base de données [influxDB](./influxDB.md) pour stocker les données de Prometheus en choisissant les données a conserver et supprimer les données trop anciennes.

17. Créer un serveur [NFS](./nfs.md) pour stocker les fichiers d'une VM (serveur web) avec montage du point de montage sur la VM.

18. Script ansible pour creer un utilisateur sur chaque serveur.

19. Se connecter a chaque serveur pour connaitre les ports utilisé, les services actifs, la version du systeme.

20. Identifier et différencier les services systemes des services applicatifs.

21. Trouver les liens entre les serveurs avec des interdépendance.

=================================================================


## à faire

- configurer influxDB pour la retention des données
    - choisir qu'elle données de prometheus conserver
    - supprimer les données trop anciennes

- deploiement de vm automatique
    - ansible
    - virt-manager

- ajouter une sécurité desactiver toutes connexion en mode root
    - ajout fail2ban connexion sudo/su root

- config ssh
    - choisir les log enregistré par l'application
    - regarder regle taille de mot de passe obligatoire pour ssh

- stat logs
    - cron tab qui genere des stats a partir des logs
    - potentiel envoie de mail si un probleme recurent

- prometheus
    - exporter de dépendance réseaux

- cartographie
    - observer dépendance

- dico python avec liste des noms de services pour chaques version

- deploiment crocc / astragale
    - openstack
    - ansible-vault
    - docker swarm
    - kafka

- apprendre stack deploiment
    - vagrant
    - ansible
    - terraform
    - docker swarm / kubernetes


# Stage

## rapport de stage

- mettre Larramendy sur la page de garde
- lexique au début au lieu de la fin
- envoyer message classe / mail anita pour 
    - charte graphique page de garde
    - résumé anglais + francais 4ème couverture
- rapport technique
    - si graphique trop long mettre en annexe
    - encadré developpement d'une partie techinque

## analyse du sujet

- problematique migration vers le cloud
    - centralisation des serveurs web
        - pour economiser de la bande passante / des ip publique / des VM
            - 1 proxy + 1 serveur web (avec découpage en sous domaine ou virtualhost)
            - DNS, configuration ip privé statique ?
        - pour assurer la continuité de service
            - 1 proxy + plusieurs serveurs web sur plusieurs VM
    - centralisation des données
        - pour economiser des VM
            - 1 serveur de base de données
        - pour assurer la continuité de service
            - 1 serveur de base de données par VM / service
    - sauvegarde
        - resiliance
            - sauvegarde sur plusieurs serveurs ( 1 LMGC 1 cloud)
            - entrave la syncronisation des données avec des sauvergades externes
        - montage NFS apartir d'un serveur de sauvegarde
        - deploiment automatique
            - si perte de continuité de service
                - pouvoir relancer une VM avec ses services configuré (avec sa config)
                    - a partir d'image qcow2
                        - implique un stockage (lourd)
                    - a partir d'un script ansible
                        - implique un versionnement des scripts
                - config implique (restauration du partage des données)
                    - montage NFS
                    - connexion BDD
                    - config proxy
    - cout/consommation (en €)
        - dépend de Ram/coeur par VM
        - rentabilise la transition écologique


## rapport technique

on m'a demandé d'analyser une architecture deja en place et non d'en deployer une

- developpement technique -> comment ? pourquoi ? donc ->
    - qu'est ce qu'un bridge
        - a quoi sers un bridge
        - utiliser la carte réseaux de l'hote pour positionner la VM sur le meme réseaux que l'hote
    - a quoi sert ansible
        - deploiement automatique
        - utiliser parametre optionnel prévu pour palier aux heterogeneité de l'infrastructure (différence de version et d'OS)

- methodologie d'analyse -> comment ? pourquoi ? donc ->
    - comment enumerer services
        - systemctl --type=service --state=active
        - problématique ubuntu 14 
            - service --status-all
    - comment trouver les interdépendance entre machine/VM
        - fouiller fichier de config
            - postgresql (HBpostgresql)
                - pg_hba.conf
            - nfs (planb)
                - /etc/exports
        - lister les ports utilisé
            - nmap <HOST\>
            - netstat -ntaup

- problemes rencontrés (comment les résoudre) -> comment ? pourquoi ? donc ->
    - Setup KVM -> filtrage par MacAdresse
        - concertation tuteur
        - lecture de doc
        - examination de l'outil DSI tools (gereé par l'UM)
    - Setup Premtheus/grafana/alert/influxdb ->  Alert manager
        - usage du server de l'UM sansa authentification
        - ssmtp obselete
    - Enumération Service -> Ubuntu 14
        - rechercher alternative pour les lister les services sans Systemd
    - Liste interdépeance -> Cumulus/Bareos a quoi sers le serveur apache
        - lecture de doc (beaucoup de doc)
        - locate/fouillage config du serv
        - découverte bareos-webui
        - chercher fichier config
            - /etc/bareos:/etc/bareos-webui
        - comprendre que le serveur web utilise bareos
            - http://<HOST\>/bareos-webui/


## soutenance

- 20 min
- 1min par diapo donc 20 diapo max
- expliquer partie technique
    - expliqué partie moin détail   lé dans le rapport
    - faire apprendre des choses en plus du rapport
- pas passer trop de temps sur la présentation du plan


## Note

playbook haproxy
reboot des VM/routeur -> prevoir reconfig haproxy avec dns
openstack deploiment 1 ou 2 site


haproxy
- sticky table/session


certbot
- que pour des ip publique ?
- 1 par os ?

dhcp
- ip dynamique 
- reconnaissance par hostname

## TODO IMPORTANT

- se connecter a openstack
```shell
source app-cred-florimond-stage-lmgc-openrc.sh
openstack server list
```

- creer cle 

```shell
openstack keypair list
openstack keypair create
```

- mettre clée en variable playbook
    - file
        - playbook / vars / server list
        - playbook / add_servers.yml

- regarder comment passé tag + group + metadonnée
    - creation des group -> add_servers.yml
        - serversweb
        - back
        - front
        - docker (astragal + elabFTW)
    - tag + metadonnée -> server_list.yml
        - tag = liste
        - metadonnée = dico

- reagrder quoi passé en variable dans server_list.yml
    - nom
    - flavor
    - image
    - network
    - security group
    - keypair
    - tag
    - metadonnée

- regarder si on peut generer inventaire avec ansible
- generer inventaire avec le script python (avec les groupes d'host ansible)

OPENSTACK VERSION >= 1 

add_servers.yml -> changer network public1 par lmgc_net


creer token git lab (astragal) pour clone le repo
- ansible vault pour le token


=============================================

creer serveur
    - proxy
    - web
    - bastion

ajout ip publique (pour ce qui ont en besoin)
    - 1 seul serveur

importer clée 
    - sur tous les serveurs

ansible vault token git lab [ astragal ]


=============================================

- make inventory
    - ajout server group
        - ajout avec ansible -> besoin de l'uuid, comment le recuperer ?
        - se baser sur les security group pour l'inventaire ? (groupe default genant, pour les groupes back un security groupe sans regles ?)

    - ajout tags
        - besoin du port_name ?

    - utiliser les META DONNEE -> LE PLUS SIMPLE

ajout floating ip
- repartir d'une config vierge
- declarer les addresse publique (les reassocier au DNS ?) 
- assigner les addresse publique


plus acces au bastion-test
- port 22 fermé (sortis du security group ?)
- ping marche pas non plus

=============================================

- associate floating ip
    - ne marche pas -> "unable to find a port for server 1a3edb04-6d2d-42f3-8ca9-4bc22d031faf"

- make inventory
    - script python marche pas parce que il y a des serveurs qui n'ont pas de meta data group (elab, serv deja en place ...)
        - ajouté groupe inventaire sans groupe
        - ajouter serveur dans plusieurs groupes (bdd, serverweb, back, front)

    - meta data -> list of key=value && not key=(value1, value2, value3, ...)

- ansible vault
    - test key ssh

