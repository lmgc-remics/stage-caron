# KVM

## KVM Installation Documentation

https://www.linuxtechi.com/how-to-install-kvm-on-ubuntu-22-04/?utm_content=cmp-true

### 0. Verify Hardware Virtualization Support

    - egrep -c '(vmx|svm)' /proc/cpuinfo

output > 0 means that your CPU supports hardware virtualization.

check if kvm already install

    - kvm-ok

if command unknwon, install cpu-checker

    - sudo apt install -y cpu-checker


> INFO: /dev/kvm exists
> KVM acceleration can be used


### 1. Install KVM

To install KVM, run the following command:

    - sudo apt install -y qemu-kvm virt-manager libvirt-daemon-system virtinst libvirt-clients bridge-utils

### 2. Unable virtualisation daemon

    - sudo systemctl enable --now libvirtd
    - sudo systemctl start libvirtd

allow user to manage vm

    - sudo usermod -aG kvm $USER
    - sudo usermod -aG libvirt $USER

### 3. Create bridge network between vm and host

    - sudo nano /etc/netplan/01-netcfg.yaml

**cf [KVM/Config/bridge_config.yaml](./KVM/Config/bridge_config.yaml)**

apply config

    - sudo netplan apply
    - sudo systemctl restart systemd-networkd

### 4. Create VM via GUI

- VM BDD & Appli
    - Ubuntu Server 22.04.2
        - Memoire (RAM) 4096 
        - CPU 2
        - stockage 50,GO

- VM BDD

    - ip 162.38.53.151
    - mac Adresse 52:54:00:5f:27:4e

- VM Appli

    - ip 162.38.53.152
    - mac Adresse 52:54:00:80:14:08

#### Note

domaine de recherche : univ-montp2.fr


## Backup and restore VM

get image in /var/lib/libvirt/images

## Create VM via la CLI

[github](https://gitlab.com/xavki/presentations-kvm-libvirt-fr/-/blob/master/5-virsh-premiere-vm/slides.md)

```shell
virt-install --name vmautomatique --description "vm en ligne de commande" --ram=4096 --vcpus=2 --disk path=/var/lib/libvirt/images/vmautomatique.qcow2,bus=virtio,size=20 --network bridge=br0  --location ~/Downloads/ubuntu-22.04.2-live-server-amd64.iso --vnc --noautoconsole --extra-args console=ttyS0
```

## Mount qcow2 image

[github](https://gist.github.com/pshchelo/6ffabbffaedc46456b39c037d16e1d8c)

**cf [KVM/mount_qcow2.sh](./KVM/mount_qcow2.sh)**

Executer le script en sudo
Puis executez les commandes suivantes

```shell
sudo chroot /mnt
ping google.fr
# si le ping echoue
exit
sudo cp /etc/resolve.conf /mnt/etc/resolve.conf
sudo chroot /mnt
apt update # ne pas utiliser sudo !!
```

## Unmount

**cf [KVM/unmount_qcow2.sh](./KVM/unmount_qcow2.sh)**

## Resize qcow2 image

Info sur l'image (commande a effectuer sur l'hote)

```shell
sudo qemu-img info /var/lib/libvirt/images/appli.qcow2
sudo qemu-img resize /var/lib/libvirt/images/appli.qcow2 +10G
```

Dans la VM

```shell
sudo parted /dev/vda print free # put Fix
sudo parted -s -a opt /dev/vda "resizepart 3 100%"

sudo pvresize /dev/vda3
sudo lvextend -l +100%FREE /dev/mapper/ubuntu--vg-ubuntu--lv 

sudo resize2fs /dev/mapper/ubuntu--vg-ubuntu--lv 
```

## VIRSH

listez les VM 

```shell
virsh list
```

start VM

```shell
virsh autostart appli
virsh start appli
```

stop VM

```shell
virsh shutdown appli
```
