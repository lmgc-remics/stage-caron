# InfluxDB

## Installation

```shell
sudo apt install influxdb
sudo systemctl enable influxdb
sudo systemctl start influxdb
```

## Login to InfluxDB

```shell
influx -username <username> -password <password>
```

Ou passez par le client web sur `http://localhost:8086`.

## Create a user

```shell
curl -X POST "http://localhost:8086/query" --data-urlencode "q=CREATE USER user_one WITH PASSWORD 'mypassverysure' WITH ALL PRIVILEGES"
```

## Allow authentication only

Changer la configuration de InfluxDB pour n'autoriser que l'authentification dans le fichier `/etc/influxdb/influxdb.conf`.

```conf
[http]
 auth-enabled = true
```

## Create a database

```shell
curl -G http://localhost:8086/query -u user_one:mypassverysure --data-urlencode "q=CREATE DATABASE prometheus"
```

## Convertir des timestamps

Convertis `1683201707090000000`  EN  `jeu. 04 mai 2023 14:01:47 CEST`

```shell
date -d @$(echo "1683201707090000000" | cut -c1-10)
```
