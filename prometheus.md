# Prometheus

[Documentation très utile](https://gitlab.com/xavki/presentation-prometheus-grafana)

## Installation

```shell
sudo apt install prometheus
```

## Démarrage du serveur

```shell
sudo systemctl start prometheus
```

## Install Node Exporter

Les exporter vont permettre de récupérer des données sur les machines.
Prometheus va les interroger pour récupérer les données.

[Liste exporters](https://github.com/prometheus/prometheus/wiki/Default-port-allocations)

```shell
sudo apt remove prometheus-node-exporter -y && sudo apt purge prometheus-node-exporter -y
```

**cf [Prometheus/install_node_exporter.sh](./Prometheus/install_node_exporter.sh)**

## Configuration

Config dans `/etc/prometheus/prometheus.yml`

**cf [Prometheus/prometheus.yml](./Prometheus/prometheus.yml)**

### Ajouter exporter

```yaml
scrape_configs:
  - job_name: 'node'
    static_configs:
      - targets: 
        - 'localhost:9100'
        - '162.38.53.151:9100'
        - '162.38.53.152:9100'
        - '162.38.53.134:9100'
```

### Ajouter regles

Les alertes vont permettre d'utiliser des raccourcis pour les expressions.
Et envoyer des mail en cas de verification positive d'une condition avec Alertmanager.


```yaml
rule_files:
  - 'rules/cpu_node.yml'
  - 'rules/alert_traffic_error.yml'
  - 'rules/alert_full_load.yml'
```


## Installer Alertmanager

https://developer.couchbase.com/tutorial-configure-alertmanager

Config dans `/etc/alertmanager/alertmanager.yml`

Ajouter des alertes correspondantes aux regles Prometheus.
La configuration suivante dépend du logiciel SSMTP pour envoyer des emails.

**cf fichier [Prometheus/alertmanager.yml](./Prometheus/alertmanager.yml)**

Ajouter config dans `/etc/prometheus/prometheus.yml`

```yaml
alerting:
  alertmanagers:
  - static_configs:
    - targets: ['localhost:9093']
```


## Requetes Prometheus pertinentes


Filesystem space available

```text
node_filesystem_avail_bytes{device!~'rootfs'}
```

Network Traffic Errors

```text
irate(node_network_receive_errs_total[1m])
```

Hardware temperature monitor

```text
node_hwmon_temp_celsius
```

## Usage d'une BDD InfluxDB

Ajout les configurations dans `/etc/prometheus/prometheus.yml`

```yaml
remote_write:
  - url: "http://localhost:8086/api/v1/prom/write?db=prometheus&u=user_one&p=mypassverysure"

remote_read:
  - url: "http://localhost:8086/api/v1/prom/read?db=prometheus&u=user_one&p=mypassverysure"
```

