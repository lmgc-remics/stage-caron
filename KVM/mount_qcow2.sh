#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <image.qcow2>"
    exit 1
fi

if [ ! -f $1 ]; then
    echo "File not found: $1"
    exit 1
fi

sudo modprobe nbd max_part=8

sudo qemu-nbd --connect=/dev/nbd0 $1

sudo fdisk /dev/nbd0 -l
sudo vgscan
sudo lvscan
sudo vgchange -ay

sudo mount $(ls /dev/mapper/ubuntu*) /mnt
sudo mount -t proc /proc /mnt/proc
sudo mount -t sysfs /sys /mnt/sys
