create user admin_one login password 'mypassverysure';
create database bdd_one;
grant all on database bdd_one to admin_one;

create user admin_two login password 'mypassverysuretoo';
create database bdd_two;
grant all on database bdd_two to admin_two;


create table personnes (
    id serial primary key, 
    nom varchar(64), 
    prenom varchar(64), 
    age integer
);

-- BDD 1 

insert into personnes(nom, prenom, age) values 
    ('user', 'one', 12),
    ('john', 'son', 45);

-- BDD 2

insert into personnes(nom, prenom, age) values 
    ('user', 'two', 43),
    ('jack', 'son', 72);



-- connect to bdd_two with postres user
-- sudo -i -u postgres 
-- psql bdd_two
alter user admin_two createrole;
-- connect to bdd_two
-- psql -U admin_two -h 162.38.53.151 bdd_one
create user user_two login password 'mypassverysurelikeadmin';
grant select on personnes to user_two;
grant insert on personnes to user_two;
-- to use auto increment
grant usage on personnes_id_seq to user_two;


-- pg_hba.conf
-- host    bdd_one    admin_one   162.38.53.128/25    md5
-- host    bdd_two    admin_two   162.38.53.128/25    md5
-- host    bdd_two    user_two    162.38.53.128/25    md5