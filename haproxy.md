# Haxproxy

## Install

```shell
sudo apt install haproxy
```

## Config

Le fichier de configuration se trouve dans `/etc/haproxy/haproxy.cfg`

Il se découpe en 4 sections :
- global
- defaults
- frontend
- backend

Pour vérifier que la syntaxe du fichier est valide:

```shell
haproxy -c -f /etc/haproxy/haproxy.cfg
```

Pour recharger la configuration:

```shell
sudo systemctl reload haproxy
```

## Configurer un loadbalancer 1/2

A ajouter à la fin du fichier  `/etc/haproxy/haproxy.cfg`

```plain
frontend my_front
	bind *:80
	default_backend my_back

backend my_back
	balance roundrobin
	server srv_php 162.38.53.151:8080
	server srv_apache 162.38.53.152
```

A chaque requete sur l'adresse du serveur HaProxy, la requete sur redirigé une fois sur le serveur PHP, une fois sur le serveur Apache. (selon la technique de balance roundrobin)

## Configurer plusieurs URL

A ajouter à la fin du fichier  `/etc/haproxy/haproxy.cfg`

- Avec subfolder

```plain
frontend my_front
    bind *:80
    mode http

    acl url_php path_beg  -i /php/
    use_backend my_back_php if url_php

    acl url_apache path_beg -i /apache/
    use_backend my_back_apache if url_apache

backend my_back_php
    server srv_php bdd.lmgc:8080

backend my_back_apache
    server srv_apache appli.lmgc
```

- Avec subdomain

```plain
frontend my_front
    bind *:80
    mode http

    acl url_php hdr(host) -i php.lmgc
    use_backend my_back_php if url_php

    acl url_apache hdr(host) -i apache.lmgc
    use_backend my_back_apache if url_apache

backend my_back_php
    server srv_php bdd.lmgc:8080

backend my_back_apache
    server srv_apache appli.lmgc
```

Pour apache configurer la route ou le virtualhost ?

## Garder l'ip du client sur le serveur final

Ajouter l'options `forwardfor` dans la section dans votre frontend.

```conf
frontend my_front
    bind *:80
    mode http

    option forwardfor
```

## Page de statisque

Toujours dans le fichier de configuration, ajouter les lignes suivantes:

```plain
listen stat
    bind *:4444
    stats enable
    stats uri /stats
    stats refresh 300s
    stats auth july:pass
```

Pour accéder à la page de statistique, il faut aller sur l'adresse du serveur HaProxy sur le port 4444 et la route `/stats`. En vous connectant avec les identifiants `july:pass`.

