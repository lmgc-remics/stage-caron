# Fail2ban

## Installation

```shell
sudo apt install fail2ban
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
vim /etc/fail2ban/jail.local
```

Dans le fichier *jail.local* mettre surcharger la
configuration. Par exemple :
```text
[DEFAULT]

ingoreip = 162.38.52.0/24 162.38.53.0/25 162.38.54.0/24

maxretry = 4

[sshd]
enabled = true
bantime = 1d
```

Puis redémarrage du service :
```shell
sudo service fail2ban restart
```

## Débannir une ip:

Info prises de [là](https://serverfault.com/questions/285256/how-to-unban-an-ip-properly-with-fail2ban).

On peut vérifier si une ip est bien bannie en faisant :
```shell
fail2ban-client status sshd | grep xxx.xxx.xxx.xxx
```

Juste toutes les adresses ip sont sur une même ligne et elle peut
être longue en fonction du serveur.

Donc on peut plutôt, pour les avoir ligne par ligne, lancer :
```shell
iptables -L -n | grep xxx.xxx.xxx.xxx
```
L'adresse est banni si présente et avec *REJECT* devant.

Enfin pour débannir l'adresse ip en question on peut faire
(pourvu que fail2ban-client soit suffisamment récent):
```shell
fail2ban-client set sshd unbanip xxx.xxx.xxx.xxx
```

En fait *sshd* est ici le nom de la geôle.

## Configration d'une action avec envoie de mail

Créer un fichier dans `/etc/fail2ban/jail.local`.

```text
[DEFAULT]
maxretry = 3
findtime = 60
sender = fail2ban@mail.com
sendername = Fail2ban
mta = sendmail


[appli_php]
enabled	= true
filter	= appli_php
logpath	= /var/log/appli_php.log
bantime	= 10m
action = iptables[name=appli_php, port=80, protocol=tcp]
         sendmail-whois[name=appli_php, dest=florimond.caron@etu.umontpellier.fr]
```

## Filtre

Créer un fichier de filtre dans `/etc/fail2ban/filter.d/appli_php.conf`.

Recuperer l'ip pour l'iptables avec le tag `<HOST>`.
 

```text
[INCLUDES]

before = common.conf

[Definition]

failregex = <HOST>:.*:false$
datepattern = ^%%Y-%%m-%%d %%H:%%M:%%S
ignoreregex =
```

## Tester un filtre

les chemins absolus sont obligatoires

```shell
fail2ban-regex /var/log/appli_php.log /etc/fail2ban/filter.d/appli_php.conf
```


## Info sur les appli des filtres deja existants

asterisk : autocommutateur téléphonique, il permet notamment la messagerie vocale

bitwarden : gestionnaire de mots de passe, ces éléments sont protégés par un seul et unique mot de passe

centreon : supervision informatique

cyrus : serveur de messagerie electronique

directadmin : anneau de contrôle d'hébergement Web graphique permettant l'administration de sites Web via un navigateur Web

dovecot : serveur IMAP et POP3

dropbear : client/serveur ssh léger

drupal : gestion de contenue (style wordpress)

ejabberd : serveur Jabber/XMPP et agent MQTT libre de messagerie instantanée à haute performance. 

exim : serveur de messagerie electronique

freeswitch : serveur pour la communication en temps reele

froxlor : panneau de contrôle d'hébergement Web.

grafana : outil de visualisation et d'analyse de données

group-office : agendas/gestion relation/mail/gestion de projet

guacamole : passerelle de bureau a distance

haproxy : load balancer

horde : style groupware group-office

kerio : serveur de messagerie pour entreprise/pro

lighttpd : serveur web léger et secure

monit : surveillance des services locaux

nagios : serveillance systeme reseaux + service

nsd : DNS

openhab : domotique connections a distances

pam : acces/authentification

portsentry : empeche le scan de port

postfix : serveur mail

proftpd : serveur FTP

roundcube : client IMAP

scanlogd : port scanner

selinux : ajoute securité pour l'acces d'element d'un systeme linux

sieve : filtrage d'email

slapd : LDAP

softethervpn : VPN

sogo : groupware

squid : serveur permettant de relayer les requetes FTP/HTTP .. comme un proxy mais un seul process asynchrone

stunnel : tunnel TLS/SSL

suhosin : patch de sécurité PHP

tine20 : progiciel groupware, Customer Relationship Management

traefik : reverse proxy, accede a une ip privée avec une ip publique (style haproxy)

webmin : appli web pour administrer un serveur unix

xinetd : controle l'acces a des services

znc : videur de reseaux IRC

zoneminder : vidéo surveillance

