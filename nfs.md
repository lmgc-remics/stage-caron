# NFS

[Documentation](https://doc.ubuntu-fr.org/nfs)

162.38.53.149 : IP du serveur (qui envoie les fichiers)
162.38.53.152 : IP du client (qui recoit les fichiers)

## Serveur Side 

### Installation

```bash
sudo apt install nfs-kernel-server
```

### Configuration

Les configurations de partage se trouvent dans le fichier */etc/exports*

```bash
echo "/home/caron/serveurWEB 162.38.53.152(rw,sync,no_subtree_check)" >> /etc/exports
sudo systemctl restart nfs-kernel-server
```

Pour gerer les acces des ip
- /etc/hosts.allow
- /etc/hosts.deny

## Client Side

### Installation

```bash
sudo apt install nfs-common
```

### Configuration

montage automatique (neccessite le demarrage du serveur)

```bash
echo "162.38.53.149:/home/caron/serveurWEB /var/www/html nfs defaults,user,auto,_netdev,bg 0 0" >> /etc/fstab
sudo mount -a
```

montage manuel

```bash
sudo mount -t nfs 162.38.53.149:/home/caron/serveurWEB /var/www/html
```

## Modification

Si l'on veut modifier le chemin d'un montage, il faut : 
- DEMONTER la partie montée
    - *sudo umount -l /var/www/html*
- Modifier la configuration coté client ET serveur
- Remonter le chemin (manuel ou fstab)