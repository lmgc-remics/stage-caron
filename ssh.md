# SSH

[Echange de cléfs](https://doc.ubuntu-fr.org/ssh)

[Bastion ssh](https://blog.octo.com/le-bastion-ssh/)

## Installation du server

```shell
sudo apt install openssh-server
```

## Demarage du serveur

```shell
sudo service sshd start
```

## Rebond

```shell
ssh -J caron@162.38.53.134 user@162.38.53.152
```

## Executer Appli graphique

```shell
ssh -X caron@162.38.53.134 code .
```

## Copier des fichiers a travers un session

De l'hote vers le serveur

```shell
scp fichier.txt caron@162.38.53.134:/home/caron
```
Du serveur vers l'hote

```shell
scp caron@162.38.53.134:/home/caron/file.txt .
```

Pour un répertoire utiliser -r

```shell
scp -r directory caron@162.38.53.134:/home/caron
```

## Port forward

Permet de mapper le port d'un serveur avec un port local

Usage : ssh -L <local_port>:<local_addr>:<server_port> user@<serv_addr>

```shell
ssh -L 8080:localhost:80 cumulus
```

## Connections par clés 

### Generer clés (coté client)

```shell
ssh-keygen -t ed25519
```

### Mettre les droits appropriés (coté client et serveur)

```shell
chmod 600 ~/.ssh
chmod 400 ~/.ssh/id_*
```

### Ajouter clé privée à l'agent ssh (coté client)

```shell
ssh-add id_xxx
```

En cas d'erreur 

    - Could not open a connection to your authentication agent.

Exécuter

```shell
eval "$(ssh-agent)"
```

Réajouter la clé privée à l'agent ssh. Puis vérifier l'ajout à la liste

```shell
ssh-add -l
```

### Stocker clé publique (coté serveur)

Via scp ou ssh-copy-id

```shell
cat id_xxx.pub >> authorized_keys
```

### Desactiver connections par mot de passe (coté serveur)

Mettre *PasswordAuthentication no* au lieu du yes

```shell
sudo nano /etc/ssh/sshd_config
```

redémarrer le serivce (coté serveur)

```shell
sudo systemctl restart sshd
```

## Config methode de connexion

Dans le fichier *~/.ssh/config*

```text
Host july
  Hostname 162.38.53.134
  IdentityFile ~/.ssh/id_ed25519
  User caron

Host Jappli
  ProxyJump july
  Hostname 162.38.53.152
  IdentityFile ~/.ssh/id_ed25519
  User user
```

Avec id_ed25519 la clé privée
