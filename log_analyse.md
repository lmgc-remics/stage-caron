# Analyse de log

## Generation blacklist d'ip

A partir de [Feodotracker](https://feodotracker.abuse.ch/blocklist/)

```shell
curl "https://feodotracker.abuse.ch/downloads/ipblocklist_recommended.json" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sort | uniq
```

## Auth.log

### Connections valides

#### Nombre d'ip qui s'est deconecté du root

```shell
cat auth.log | grep "Disconnected from authenticating user root" | cut -d ' ' -f 11 | sort | uniq | wc -l
```

#### Nombre de connections accepté

```shell
cat auth.log | grep Accepted | wc -l
```

#### Listes des utilisateur accepté (par connexion ssh)

```shell
cat auth.log | grep Accepted | cut -d ' ' -f 9 | sort | uniq
```

#### Listes des connexion avec systemd (month:day:hour:user) 

```shell
cat auth.log | grep "systemd" | grep -v Removed | grep -v pam | cut -d ' ' -f 1,2,3,11
```

### Connections Invalide

#### Filtre Invalid

```shell
cat auth.log | grep Invalid > malicious_connection.txt
```

#### Get IP && User

```shell
cat malicious_connection.txt | cut -d ' ' -f 8 > user_dico.txt
cat malicious_connection.txt | cut -d ' ' -f 10 > ip_dico.txt
```

#### Sort && Delete Repetions

```shell
sort user_dico.txt | uniq > user.txt
sort ip_dico.txt | uniq > ip.txt
```

#### tentatives de connections (date:user:ip) 

```shell
cat auth.log | grep Invalid | cut -d ' ' -f 1,2,3,8,10
```

-> statistique tentatives ip/heure

## nginx_gitlab_access.log

### filtrer doublons

```shell
cat nginx_gitlab_access.log  | sort | uniq
```

### Connections valides

#### Extraires tous les emails

```shell
cat nginx_gitlab_access.log  | sort | uniq | grep -o '[[:alnum:]+\.\_\-]*@[[:alnum:]+\.\_\-]*' | sort | uniq
```

#### (ip:user:date:mode d'acces)

```shell
cat nginx_gitlab_access.log  | sort | uniq | cut -d ' ' -f 1,4,3,12 | sort | uniq
```

