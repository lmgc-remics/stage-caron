# Virtual Env

## DOCS

- [Installation](https://virtualenvwrapper.readthedocs.io/en/latest/install.html)
- [Usage](https://virtualenvwrapper.readthedocs.io/en/latest/command_ref.html#command-workon)


## Installation

```shell
sudo apt install python-is-python3 -y
python -m pip install virtualenvwrapper
```

## Configuration

```shell
echo "

# Virtual Env
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/local/bin/virtualenvwrapper.sh
" >> ~/.bashrc
source ~/.bashrc
```

## Créer Env

```shell
mkvirtualenv temp
cd $WORKON_HOME/temp
```

Crée un repertoire dans  `$WORKON_HOME/<nom_env>`

Pour quitter l'environnement commande `deactivate`

## Gérer Env

```shell
lsvirtualenv
showvirtualenv temp
rmvirtualenv temp
```

Pour changer d'environnement commande `workon <nom_env>`

## Temp Env

```shell
mktmpenv
```

Se supprime automatiquement apres la commande `deactivate`