#!/bin/bash

if [ $# -lt 1 ]; then
	echo "Veuillez donnez au moins un nom de fichier"
	exit 1
fi


for file in $@; do
	echo -e "FILE : $file\n"
	for ip in $(grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" $file | sort | uniq); do
		nbAppears=$(grep $ip $file | wc -l)
		if [ $nbAppears -gt 100 ]; then
			echo $ip address appears $nbAppears times
		fi
	done
done

exit 0

