# Openstack

## Installation

Il faut installer openstack et ansible dans un environnement virtuel:

```shell
mkvirtualenv ansible
python -m pip install "ansible>=2.9" "openstacksdk>=1.0" "python-openstackclient"
```

## Config

Obtenir un fichier `app-cred-florimond-stage-lmgc-openrc.sh`

```shell
mkdir ~/.openstack
mv app-cred-florimond-stage-lmgc-openrc.sh ~/.openstack
source ~/.openstack/app-cred-florimond-stage-lmgc-openrc.sh
```

## Usage

Lister les différents elements

```shell
openstack server list
openstack keypair list
openstack security group list
openstack server group list
openstack image list
openstack flavor list
openstack network list
openstack floating ip list
openstack volume list
```

## Crée clée

```shell
openstack keypair delete caron
openstack keypair create --public-key ~/.ssh/id_openstack.pub caron
```

## Mapper floating ip

Pour obtenir les ip et les id utiliser les commandes `list` citées ci-dessus.

Usage:

```shell
openstack floating ip set <floating_ip> --fixed-ip-address <fixed_ip> --port <port_id>
```

Exemple:

```shell
openstack floating ip set 195.220.237.49 --fixed-ip-address 192.168.34.5 --port 59a9d112-d539-4399-813f-9e439b35517d
```

automatisé :

```shell
name=bastion_test
openstack floating ip set 195.220.237.49 --fixed-ip-address $(openstack server list | grep $name | cut -d'|' -f5 | cut -d= -f 2 | tr -d ' ') --port $(openstack port list | grep $(openstack server list | grep $name | cut -d'|' -f5 | cut -d= -f 2 | tr -d ' ') | cut -d'|' -f 2)
```

