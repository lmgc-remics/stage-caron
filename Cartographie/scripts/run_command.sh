#!/bin/bash

liste=$1

if ! [ -f $liste ]; then

	echo Wrong file
	exit 1
fi

for line in $(cat $liste); do

	if [[ ! "$line" =~ ^# ]]; then

	        arr=(${line//:/ })
        	ip=${arr[0]}
	        hostname=${arr[1]}

		echo "$hostname : "$(ssh roroot@$ip $2)
	fi
done
