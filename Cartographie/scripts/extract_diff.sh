#!/bin/bash

nb=$1

python3 add.py -n $(($nb + 1)) -f services | cut -d '.' -f 1 > /tmp/2

python3 add.py -n $nb -f services | cut -d '.' -f 1 > /tmp/1


in=/tmp/1
extract=/tmp/2


for line in $(cat $in); do

        if ! grep $line $extract >/dev/null; then
		echo $line
	fi
done

rm -f /tmp/1 /tmp/2
