#!/bin/bash

in=$1
extract=$2

rm -f same different

for line in $(cat $in); do

        if grep $line $extract >/dev/null; then
                echo $line >> same
        else
		echo $line >> different
	fi
done

rm same
cat different
rm different
