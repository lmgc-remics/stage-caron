#!/bin/bash

for line in $(cat liste); do

        arr=(${line//:/ })
	ip=${arr[0]}
        hostname=${arr[1]}

        echo "
Host $hostname
  User roroot
  Hostname $ip
  PreferredAuthentications publickey
  IdentityFile /home/caron/.ssh/id_cartographie
  ForwardX11 yes

        "

done

