#!/bin/bash

service=$1

rm -f /tmp/have /tmp/donthave

for serv in $(cat liste | cut -d ':' -f 2); do
	if grep $service $serv/active_$serv  &> /dev/null; then
		echo $serv >> /tmp/have
	else
		echo $serv >> /tmp/donthave

	fi

done

if [ -f /tmp/have ] ; then

	echo -e HAVE"\n"
	cat /tmp/have

fi

if [ -f /tmp/donthave ] ; then

	echo -e "\n""DON'T HAVE""\n"
	cat /tmp/donthave

fi

rm -f /tmp/have /tmp/donthave

