import argparse

parser = argparse.ArgumentParser(description='add.py')
parser.add_argument("-n", "--number", help="Number to print", type=int)
parser.add_argument("-f", "--file", help="File to add", type=str)

args = parser.parse_args()

if args.file == None or args.number == None:
	print('Wrong argss')
	quit()

with open(args.file, 'r') as file:
	lines = [line.rstrip('\n') for line in file]


dico = {}
sum = int(lines[0][0] + lines[0][1])
service = lines[0].split(' ')[1]

for line in range(1, len(lines)):


	if service == lines[line].split(' ')[1]:
		sum += int(lines[line][0] + lines[line][1])
	else:
		dico[service] = sum
		sum = int(lines[line][0] + lines[line][1])

	service = lines[line].split(' ')[1]

for i in dico.keys():
	if dico[i] >= args.number:
		print(i)

