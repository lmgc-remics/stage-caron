import argparse

parser = argparse.ArgumentParser(description='find_service.py')
parser.add_argument("-s", "--service", help="Service to search for", type=str)

args = parser.parse_args()

if args.service is None:
	print('Wrong argss')
	quit()


WK = "/home/caron/Documents/Cartographie/"


with open(WK + "liste") as inv:
    servers = [line.strip().split(':')[1] for line in inv]

services = {}

for server in servers:
    with open(WK + server + "/active_" + server) as srv:
        services[server] = [line.strip() for line in srv]


service_name_variants = {
    "accounts": ["accounts-daemon"],
    "acpid": ["acpid"],
    "amsd": ["amsd"],
    "apache": ["apache", "apache2", "apache-htcacheclean", "httpd"],
    "apparmor": ["apparmor"],
    "apport": ["apport"],
    "atd": ["atd"],
    "auditd": ["auditd"],
    "autofs": ["autofs"],
    "bareos-server": ["bareos-director", "bareos-storage"],
    "bareos-client": ["bareos-fd", "bareos-filedaemon"],
    "bind9": ["bind9"],
    "blk": ["blk-availability"],
    "cgroupfs": ["cgroupfs-mount"],
    "chrony": ["chrony", "chronyd"],
    "cloud": ["cloud-config", "cloud-final", "cloud-init", "cloud-init-local"],
    "console": ["console-setup"],
    "container": ["containerd"],
    "cron": ["cron", "crond"],
    "cryptdisks": ["cryptdisks", "cryptdisks-early"],
    "dbus": ["dbus"],
    "dns": ["dns-clean"],
    "docker": ["docker"],
    "dsm": ["dsm_om_connsvc", "dsm_sa_datamgrd", "dsm_sa_eventmgrd"],
    "ebtables": ["ebtables"],
    "elasticsearch": ["elasticsearch"],
    "fail2ban": ["fail2ban"],
    "final": ["finalrd"],
    "firewall": ["firewalld"],
    "friendly": ["friendly-recovery"],
    "fusioninventory": ["fusioninventory-agent"],
    "tty": ["getty@tty1", "getty@tty2", "serial-getty@ttyS0"],
    "gitlab": ["gitlab-runsvdir"],
    "grub": ["grub-common"],
    "gssproxy": ["gssproxy"],
    "guacamole": ["guacd"],
    "gunicorn": ["gunicorn-extranet"],
    "ifup": ["ifup@eno1", "ifup@enp5s0f1", "ifup@ens3"],
    "instsvcdrv": ["instsvcdrv"],
    "irq": ["irqbalance"],
    "iscsi": ["iscsid", "iscsi-shutdown", "cpqiScsi", "open-iscsi"],
    "kdump": ["kdump"],
    "keyboard": ["keyboard-setup"],
    "kibana": ["kibana"],
    "killprocs": ["killprocs"],
    "kmod": ["kmod", "kmod-static-nodes"],
    "ksm": ["ksm", "ksmtuned"],
    "ldap": ["ldap", "libnss-ldap", "slapd"],
    "libvirt": ["libvirt-bin", "libvirtd", "virtlogd"],
    "lvm": [
        "lvm2-lvmetad",
        "lvm2-monitor",
        "lvm2-pvscan@252:2",
        "lvm2-pvscan@252:3",
        "lvm2-pvscan@252:5",
        "lvm2-pvscan@259:4",
        "lvm2-pvscan@8:1",
        "lvm2-pvscan@8:19",
        "lvm2-pvscan@8:3",
    ],
    "lxcfs": ["lxcfs", "lxd-containers"],
    "mariadb": ["mariadb"],
    "mdadm": ["mdadm"],
    "memcached": ["memcached"],
    "ModemManager": ["ModemManager"],
    "mongodb": ["mongod"],
    "mr": ["mr_cpqScsi", "mrmonitor"],
    "multipath": ["multipathd"],
    "mysql": ["mysql", "mysqld"],
    "netcf": ["netcf-transaction", "netcf-transaction.service"],
    "netvault": ["netvault", "netvaultd"],
    "network": [
        "network",
        "networkd-dispatcher",
        "networking",
        "NetworkManager",
        "NetworkManager-wait-online",
    ],
    "nfs": ["nfs-kernel-server", "nfs-server"],
    "nginx": ["nginx"],
    "nscd": ["nscd", "nslcd"],
    "ondemand": ["ondemand"],
    "packagekit": ["packagekit"],
    "plymouth": ["plymouth-quit", "plymouth-quit-wait", "plymouth-read-write"],
    "polkit": ["polkit", "polkitd"],
    "postfix": ["postfix", "postfix@-"],
    "postgresql": ["postgresql", "postgresql@14-main", "postgresql@9"],
    "pppd": ["pppd-dns"],
    "procps": ["procps"],
    "rc": ["rc-local", "rc.local"],
    "resolvconf": ["resolvconf"],
    "rhel": ["rhel-dmesg", "rhel-domainname", "rhel-import-state", "rhel-readonly"],
    "rpc": ["rpcbind", "rpc-statd"],
    "rsync": ["rsync", "rsyslog"],
    "screen": ["screen-cleanup"],
    "seafile": ["seafile", "seahub"],
    "sendmail": ["sendmail"],
    "sendsigs": ["sendsigs"],
    "setvtrgb": ["setvtrgb", "setvtrgb.service"],
    "smad": ["smad", "sm-client"],
    "snap": ["snap", "snapd", "snapd.seeded"],
    "snmpd": ["snmpd", "snmptrapd"],
    "ssh": ["ssh", "sshd"],
    "systemd": [
        "sudo",
        "sysstat",
        "systemd-journald",
        "systemd-journal-flush",
        "systemd-logind",
        "systemd-machined",
        "systemd-modules-load",
        "systemd-networkd",
        "systemd-networkd-wait-online",
        "systemd-random-seed",
        "systemd-readahead-collect",
        "systemd-readahead-replay",
        "systemd-remount-fs",
        "systemd-resolved",
        "systemd-sysctl",
        "systemd-sysusers",
        "systemd-timesyncd",
        "systemd-tmpfiles-setup",
        "systemd-tmpfiles-setup-dev",
        "systemd-udevd",
        "systemd-udev-settle",
        "systemd-udev-trigger",
        "systemd-update-utmp",
        "systemd-user-sessions",
        "systemd-vconsole-setup",
    ],
    "tomcat": ["tomcat8", "tomcat9"],
    "tuned": ["tuned", "tuned-upgrade"],
    "ubuntu-fan": ["ubuntu-fan", "ubuntu-fan-smart"],
    "udev": ["udev", "udev-finish", "udevmonitor", "udevtrigger"],
    "udisk": ["udisks2", "udisksd"],
    "ufw": ["ufw", "ufw-framework", "ufw-reload", "ufw-user-input"],
    "umount": ["umountfs", "umountnfs.sh", "umountroot", "unattended-upgrades"],
    "upower": ["upower", "upowerd"],
    "urandom": ["urandom", "ureadahead", "ureadahead-other"],
    "user": [
        "user@0",
        "user@1001",
        "user@1002",
        "user@1003",
        "user@1004",
        "user@302",
        "user@318",
        "user@325",
        "user-runtime-dir@0",
        "user-runtime-dir@1001",
        "user-runtime-dir@1002",
    ],
    "uuidd": ["uuidd"],
    "wpa": ["wpa_supplicant"],
    "1x11": ["x11-common"],
    "zed": ["zed"],
    "zfs": [
        "zfs-fuse",
        "zfs-import-cache",
        "zfs-load-module",
        "zfs-mount",
        "zfs-share",
        "zfs-volume-wait",
        "zfs-zed",
    ],
}



if args.service not in service_name_variants.keys():
    print("Service not found")
    quit()


have = []


for server in services.keys():
    for service in service_name_variants[args.service]:
        if service in services [server]:
            have.append(server)


print("HAVE\n")
[print(server) for server in have]
print("\nDON'T HAVE\n")
[print(server) for server in servers if server not in have]
