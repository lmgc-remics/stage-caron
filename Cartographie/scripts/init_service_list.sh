#!/bin/bash

wk=$(pwd)
state=$1

for line in $(cat $wk/liste); do

	if [[ ! "$line" =~ ^# ]]; then

		arr=(${line//:/ })
		ip=${arr[0]}
		hostname=${arr[1]}
		echo $hostname
		mkdir -p $wk/$hostname
# 		ssh roroot@$ip "systemctl list-units --type=service --state=$state | head -n -6 | tail -n +2 | tr -d ' ' | cut -d '.' -f 1 " > $wk/$hostname/$state"_"$hostname
		ssh roroot@$ip "systemctl list-units --type=service | head -n -6 | tail -n +2 | tr -d ' ' | cut -d '.' -f 1 " > $wk/$hostname/$state"_"$hostname

	fi
done

