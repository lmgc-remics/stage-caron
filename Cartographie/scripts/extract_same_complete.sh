#!/bin/bash


in=$1
extract=$2

for line in $(cat $in); do

	if grep $line $extract >/dev/null; then
		echo $line >> /tmp/extract
	fi
done

for line in $(cat $extract); do

	if ! grep $line /tmp/extract  >/dev/null; then
		echo $line
	fi

done

rm /tmp/extract
