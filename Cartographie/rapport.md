# Cartographie

planb : Ubuntu 14.04.6 LTS
masterball : Ubuntu 14.04.5 LTS

ubuntu 14 ne possede pas systemd && systemctl

nimbus : Ubuntu 16.04.7 LTS
donald : Ubuntu 16.04.7 LTS
cumulus : Ubuntu 16.04.7 LTS

# Port scan

guacamole : 162.38.54.1 

port 80 ouvert
-> guacamole works

reservation : 162.38.54.13

port 80 ouvert
apache default page

# Log

chemin des backup

```shell
cat /var/log/syslog | grep "root@"
```


## tried on servers

### inspect
hyperball
- virsh list --all # empty list
- ls /var/lib/libvirt/images            # permission denied

masterball
- virsh list --all                      # permission denied

### services

lister les services actifs

```bash
systemctl list-units --type=service | cut -d ' ' -f 1 | head -n -7 | tail +2 > service_list
```

# deroulement

## os_versions

../script/run_command "hostnamectl | grep -i operating" | cut -d ':' -f 1,3

## liste de services

../script/init_service_list 

## services unique

cd ~/Documents/Cartographie
d=glpi
../script/extract_same $d/active_$d ~/Documents/stage-caron/Cartographie/services_sys.conf > $d/services; nano $d/services

## listes des services uniques

for i in */services ;do echo ------------------------------------------"$i";cat $i ;done

## audit.md generation

for i in */services ;do echo -e  "\n""## $i""\n";while read -r lines; do echo "- "$lines;done < $i ;done > ~/Documents/stage-caron/Cartographie/audit.md

# Interdépendance

[Seafile]
## cumulus 1

- bareos server
- postgresql        # 9.5

## nimbus 2

- bareos client
- backup donald

## donald 3

- bareos client
- backup daisy

## daisy 4
ssh 
- zfs
- seafile

[Postgresql]
## HBpostgresql

- postgresql        # 14

## extranet

- gunicorn  (server web)
- use HBpostgresql

## planb

- save dans nimbus -> /zfs_data/planb

# Notes

## services

revoir hypberball / masterball / planb

## autofs && ldap

ou voir d'ou viens le partage dans la config autofs

ldap && ldap_cluster autofs non config ?

## bind9 && rpcbind

server DNS -> bind9 logbox 
- effectif ?

ldap, planb, masterball, ... -> client dns (rpcbind) ?

## PROXY

a quoi sert internet && internetold

ou sont les backend des serveur web ?
- mecamat.lmgc.univ-montp2.fr
- m3c.lmgc.univ-montp2.fr
- woodscicraft2014.lmgc.univ-montp2.fr
- cfp2013.lmgc.univ-montp2.fr
- gdr2519.lmgc.univ-montp2.fr
- mist2015.lmgc.univ-montp2.fr
- contrat.lmgc.univ-montp2.fr
- siweb.lmgc.univ-montp2.fr
- inventaire.lmgc.univ-montp2.fr
- gedonex.lmgc.univ-montp2.fr
- depotpubli.lmgc.univ-montp2.fr
- w3.lmgc.univ-montp2.fr
- remics.lmgc.univ-montp2.fr
- http://www.msoc.fr

pourquoi extranet, www , reservation, glpi et overleaf ne sont pas derriere le proxy


## BDD

regarder le nom des bd dans les serveurs

clients inconnu
- mysql (hyperball)
- MBbdd

clients a vérifier
- hbpostgresql

# Utiles

affichez les lignes ou il y a des IP

cat file | grep -E "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

ajouter le falg *-o* pour ne garder que les IP