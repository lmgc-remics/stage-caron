
## git-xen - 162.38.54.10

- bareos
- fail2ban
- gitlab
- postfix
- rpcbind
- wpa_supplicant

## extranet - 162.38.54.11

- fail2ban
- gunicorn
- nginx

## mongo - 162.38.54.129

- mongod

## reservation - 162.38.54.13

- apache2
- bareos
- fusioninventory
- mysql

## hyperball - 162.38.54.154

- bareos
- dsm_om_connsvc
- dsm_sa_datamgrd
- dsm_sa_eventmgrd
- gssproxy
- instsvcdrv
- kdump
- ksm
- ksmtuned
- libvirtd
- netcf
- postfix
- rpcbind
- tuned
- virtlogd

## planb - 162.38.54.15

- bareos-fd
- friendly-recovery
- libnss-ldap
- netvault
- nfs-kernel-server
- rpcbind

## mysql - 162.38.54.167

- apache-htcacheclean
- bareos
- fusioninventory
- mysql

## internet - 162.38.54.164

- apache-htcacheclean
- apache2
- bareos
- fusioninventory

## internetold - 162.38.54.168

- apache-htcacheclean
- apache2
- bareos
- fusioninventory
- rpcbind

## masterball - 162.38.54.171

- bareos-fd
- cryptdisks
- friendly-recovery
- libvirt-bin
- rpcbind
- zfs-fuse

## nimbus - 162.38.54.175

- bareos
- fail2ban
- zed
- zfs

## postgresql - 162.38.54.176

- postgresql
- postgresql@14

## ldap_cluster - 162.38.54.17

- autofs
- rpcbind
- slapd

## daisy - 162.38.54.18

- docker
- mariadb
- nginx
- seafile
- seahub
- zfs

## guacamole - 162.38.54.1

- apache2
- bareos
- fail2ban
- fusioninventory
- guacd
- mysql
- tomcat8

## jumpbox - 162.38.54.21

- bareos
- fail2ban
- nslcd
- wpa_supplicant

## ldap - 162.38.54.23

- autofs
- rpcbind
- slapd

## overleaf - 162.38.54.29

- docker
- fail2ban
- nginx

## cumulus - 162.38.54.30

- apache2
- bareos-storage
- fail2ban
- fusioninventory
- postgresql

## logbox - 162.38.54.3

- apache-htcacheclean
- bareos
- bind9                     (DNS)
- chrony                    (horloge sync)
- elasticsearch
- fail2ban
- fusioninventory           (inventaire parc informatique)
- kibana

## puech - 162.38.54.41

- autofs
- bareos
- chrony
- fail2ban
- libnss
- nslcd
- rpcbind
- wpa_supplicant

## proxy - 162.38.54.44

- bareos
- chronyd
- fail2ban
- httpd
- kdump
- postfix
- tuned

## donald - 162.38.54.45

- apache2
- bareos
- docker
- fail2ban
- fusioninventory
- mysql
- zed
- zfs

## www - 162.38.54.46

- apache2
- sendmail

## glpi - 162.38.54.73

- apache2
- chrony
- fail2ban
- fusioninventory
- mysql

## MBbdd - 162.38.54.78

- apache-htcacheclean
- apache2
- bareos
- fail2ban
- mysql
- rpc
- rpcbind
- wpa_supplicant

## licence - 162.38.54.8

