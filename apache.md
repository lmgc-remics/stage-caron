# Apache

## Installation

```shell
sudo apt install apache2
```

## Utiliser Php et PostgreSQL

```shell
sudo apt install php libapache2-mod-php php-pgsql
sudo service apache2 restart
```

## Activer les erreurs du serveur apache

```shell
sudo nano /etc/php/8.1/apache2/php.ini
```

changer la ligne

    display_errors = Off

puis la passer On

    display_errors = On

puis redemarrer apache pour prendre en compte la modification

```shell
sudo service apache2 restart
```

