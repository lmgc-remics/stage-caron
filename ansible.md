# Ansible

## Installation

en utilisant apt

```shell
sudo apt install ansible
```

ou en utilisant pip

```
pyhon3 -m pip install ansible
```

## Lancer une commande sur un inventaire

```shell
ansible all -i inventory.ini -m ansible.builtin.shell -a "touch ~/file"
```

## Lancer un playbook

```shell
ansible-playbook -i inventory.ini maj_updatedb.yaml
```

## Retirer [DEPRECATION WARNING]

```shell
export ANSIBLE_DEPRECATION_WARNINGS=false
```

## Afficher inventory.ini en yaml

```shell
ansible-inventory -i inventory.ini --list -y
```

## Ansible vault

Chiffrer un fichier

```shell
echo 'mypassverysure' > vault.key
# export DEFAULT_VAULT_PASSWORD_FILE=vault.key 
ansible-vault encrypt vars/keys.yml --vault-password-file vault.key 
```

Editer un fichier (sans le déchiffrer)

```shell
export EDITOR=nano
ansible-vault edit vars/keys.yml --vault-password-file ./vault.key
```

Déchiffrer un fichier (totalement)

```shell
ansible-vault decrypt vars/keys.yml --vault-password-file ./vault.key
```

Changer le mot de passe de chiffrement

```shell
ansible-vault rekey --new-vault-password-file ./new-vault.key --vault-password-file ./vault.key
```

Chiffer seulement une string (puis recopier le contenu pour la valeur d'une clée yaml)

```shell
ansible-vault encrypt_string 'secret' --vault-password-file ./vault.key 
```

Utiliser un playbook avec des lignes chiffrées

```shell
ansible-playbook -i i.ini playtest.yml --vault-password-file ./vault.key
```




