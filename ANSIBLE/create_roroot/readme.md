
Fichier *host* qui associe les ip avec des noms de serveurs

Fichier *inventory.ini* qui groupe les noms de serveurs en groupe
pour ansible.

Pour que ça marche, il faut que le fichier ~/.ssh/config permette
de faire
```shell
ssh nom_serveur
```
et que la connexion se fasse sur le compte root et sans demander de mot d epasse.

Il faut installer openstack et ansible dans un environnement virtuel:
```shell
mkvirtualenv ansible
python -m pip install "ansible>=2.9" "openstacksdk==0.62" "python-openstackclient"

Ensuite pour lancer une commande on peut faire :

```shell
ansible -m shell -a "hostname" -i inventory.ini all --ask-become-pass
ansible-playbook -i inventory.ini add_roroot.yaml --limit MB
```


Ce qui a été fait :
* ajouter l'utilisateur roroot
* ajouter roroot au groupe root
* sur cumulus, ajouté roroot au groupe postgres et bareos
* pour planb et cumulus, la création de compte s'est faite à la main
