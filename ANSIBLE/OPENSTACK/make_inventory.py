import yaml
import openstack
conn = openstack.connect()

ssh_user = 'ubuntu'
ssh_pub = '/home/caron/.ssh/id_openstack'

jump_ip = '195.220.237.49'

inventory = {
    'all': {
        'hosts': {},
        'children': {}
    }
}

for s in conn.compute.servers():
    for addr in s.addresses['lmgc_net']:
        
        if s.metadata['group'] not in inventory: 
            inventory[s.metadata['group']] = {'hosts': {}}

        inventory[s.metadata['group']]['hosts'][s.name] = {
            'ansible_host': addr['addr'],
			'ansible_user': 'ubuntu',
			'ansible_become': True,
			'ansible_ssh_private_key_file': ssh_pub,
		}

       

# write inventory
with open('inventory.yml', 'w') as yaml_file:
    yaml.dump(inventory, yaml_file, default_flow_style=False, sort_keys=False)
