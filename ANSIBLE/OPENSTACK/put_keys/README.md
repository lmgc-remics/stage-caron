# Distribuer clés

## Crée un fichier de clés

A prendre pour exemple.

```shell
echo "
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM1bZqpEN4Kdbyq1/ptH6hVsCbqqM1aleYESNPIvhJ4r caron@july
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGcclNEVM3GDbHn/RhZ6RHfQrEwTWgHSmpn2o7k2lY8a user@bdd
" > keys.txt
```

## Générer un fichier mot de passe

Uniquement pour un usage de test ou automatique (utiliser un editeur en temps normal != `inventory` )

```shell
echo 'mypassverysure' > vault.key
```

## Générer vars/keys

```shell
./keys_file_gen.sh vault.key keys.txt vars/keys.yml
```

## Executer playbook

```shell
ansible-playbook -i ../sure_inventory.yml add_keys.yml --vault-password-file vault.key
```
