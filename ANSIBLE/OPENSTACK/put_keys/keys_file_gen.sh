#!/bin/bash

if [ $# -ne 3 ];then
	echo
	echo "Usage: [vault.key] [keys.txt] [keys.yml]"
	echo
	exit 1
fi

vault_file=$1
keys=$2
out=$3

echo "---" > $out
echo "  keys:" >> $out
echo >> $out

n=1
while read key; do

	echo "    key"$n": " >> $out
	echo "      key: !vault |" >> $out
	ansible-vault encrypt_string "$key" --vault-password-file $vault_file | tail +2 >> $out

	n=$(( $n + 1 ))

done < $keys

echo >> $out

